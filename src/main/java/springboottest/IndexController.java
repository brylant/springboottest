package springboottest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Controller
class IndexController {

    private List<Book> booksRepository = new LinkedList<Book>();

    IndexController() {
        Collections.addAll(booksRepository, new Book("Book 1", "Author 1"), new Book("Book 2", "Author 2"));
    }

    @RequestMapping("/listExistingBooks")
    ModelAndView index() {
        Books bookList = new Books();
        bookList.setBooks(booksRepository);
        return new ModelAndView("index", "existingBooks", bookList);
    }

    @RequestMapping("/addBook")
    ModelAndView addBook(@ModelAttribute("books") Books bookList) {
        booksRepository.clear();
        booksRepository.addAll(bookList.getBooks());
        return new ModelAndView("index", "existingBooks", bookList);
    }
}
