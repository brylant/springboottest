$(function () {
    $("#add-book").on("click", function () {
        var existingBookList = $("#existing-book-list");
        var index = existingBookList.find("tr").length;
        var inputTitleHtml = "<input type='text' name='books[" + index + "].title'/>";
        var inputAuthorHtml = "<input type='text' name='books[" + index + "].author'/>";
        var rowHtml = "<tr><td>" + inputTitleHtml + "</td><td>" + inputAuthorHtml + "</td></tr>";
        existingBookList.find("tr:last").after(rowHtml);
    });
});
